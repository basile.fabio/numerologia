<?php

use Illuminate\Support\Facades\Route;
use App\Mail\TestEmail;
use App\Mail\TestEmailMD;
use App\User;
use App\Number;
use App\Events\NewNumberCreated;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/numboards', 'NumboardController@numboard')->name('numboards');

Route::get('testMail', function()
{
  $user = User::find(2);
  $number = Number::where('user_id', 2)->first();

  \Mail::to($user->email)->send(new TestEmailMD($user, $number));
});

Route::get('testEvent', function() {

  $number = Number::first();
  event(new NewNumberCreated($number));
});
