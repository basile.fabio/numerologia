@php
  $emptyImg = asset('/imgs/empty-img.jpg');
  $arrNumbers = optional($numbers)->number;
  $unsNumbers = unserialize(optional($numbers)->number_value);
  
@endphp

@extends('layouts.app')

@section('content')
  @if (!empty($numbers))
    <div id="numbers" class="row py-4">
    <!-- NUMERO_VITA_FREE -->
      <div class="card col-md-4 px-1 px-md-3 py-4 noBorderCard" style="color:limegreen;">
        <img src="https://picsum.photos/id/1/200/150" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title text-center mb-0">NUMERO DELLA VITA - free</h5>
          <p class="card-text h-100 text-center d-flex justify-content-center align-items-center" style="font-size:50px;">
            {{$unsNumbers['free_vita']}}
          </p>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item bg-transparent"><a href="#" style="color:limegreen;">calcolo numero della vita</a></li>
          <li class="list-group-item bg-transparent"><a href="#" style="color:limegreen;">origini numero della vita</a></li>
          <li class="list-group-item bg-transparent"><a href="#" style="color:limegreen;">altre info</a></li>
        </ul>
      </div>
      <!-- NUMERO_ANIMA -->
      <div class="card col-6 col-md-4 px-1 px-md-3 py-4 noBorderCard" style="color:{{!empty($arrNumbers['numero_anima']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">
        <img src="{{!empty($arrNumbers['numero_anima']) && $numbers->payment ? 'https://picsum.photos/id/1/200/150' : $emptyImg}}" class="card-img-top" alt="...">
        <div class="card-body px-0">
          <h5 class="card-title text-center mb-0" >NUMERO ANIMA</h5>
          <p class="card-text h-100 text-center d-flex justify-content-center align-items-center" style="font-size:50px;">
            {{!empty($arrNumbers['numero_anima']) ? $arrNumbers['numero_anima'] : "?"}}
          </p>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_anima']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">calcolo numero dell'anima</a></li>
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_anima']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">origini numero dell'anima</a></li>
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_anima']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">altre info</a></li>
        </ul>
      </div>
      <!-- NUMERO_PERSONA -->
      <div class="card col-6 col-md-4 px-1 px-md-3 py-4 noBorderCard" style="color:{{!empty($arrNumbers['numero_persona']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">
        <img src="{{!empty($arrNumbers['numero_persona']) && $numbers->payment ? 'https://picsum.photos/id/1/200/150' : $emptyImg}}" class="card-img-top" alt="...">
        <div class="card-body px-0">
          <h5 class="card-title text-center mb-0">NUMERO PERSONA</h5>
          <p class="card-text h-100 text-center d-flex justify-content-center align-items-center" style="font-size:50px;">
          {{!empty($arrNumbers['numero_persona']) ? $arrNumbers['numero_persona'] : "?"}}
          </p>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_persona']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">calcolo numero persona</a></li>
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_persona']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">origini numero persona</a></li>
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_persona']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">altre info</a></li>
        </ul>
      </div>
      <!-- NUMERO_DESTINO -->
      <div class="card col-6 col-md-4 px-1 px-md-3 py-4 noBorderCard" style="color:{{!empty($arrNumbers['numero_destino']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">
        <img src="{{!empty($arrNumbers['numero_destino']) && $numbers->payment ? 'https://picsum.photos/id/1/200/150' : $emptyImg}}" class="card-img-top" alt="...">
        <div class="card-body px-0">
          <h5 class="card-title text-center mb-0">NUMERO DESTINO</h5>
          <p class="card-text h-100 text-center d-flex justify-content-center align-items-center" style="font-size:50px;">
          {{!empty($arrNumbers['numero_destino']) ? $arrNumbers['numero_destino'] : "?"}}
          </p>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_destino']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">calcolo numero destino</a></li>
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_destino']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">origini numero destino</a></li>
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_destino']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">altre info</a></li>
        </ul>
      </div>
      <!-- NUMERO_ESPRESSIONE -->
      <div class="card col-6 col-md-4 px-1 px-md-3 py-4 noBorderCard" style="color:{{!empty($arrNumbers['numero_espressione']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">
        <img src="{{!empty($arrNumbers['numero_espressione']) && $numbers->payment ? 'https://picsum.photos/id/1/200/150' : $emptyImg}}" class="card-img-top" alt="...">
        <div class="card-body px-0">
          <h5 class="card-title text-center mb-0">NUMERO ESPRESSIONE</h5>
          <p class="card-text h-100 text-center d-flex justify-content-center align-items-center" style="font-size:50px;">
            {{!empty($arrNumbers['numero_espressione']) ? $arrNumbers['numero_espressione'] : "?"}}
          </p>
        </div>
        <ul class="list-group list-group-flush text-center">
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_espressione']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">calcolo numero espressione</a></li>
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_espressione']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">origini numero espressione</a></li>
          <li class="list-group-item bg-transparent"><a href="#" style="color:{{!empty($arrNumbers['numero_espressione']) && $numbers->payment ? '#3490dc;' : 'grey;'}}">altre info</a></li>
        </ul>
      </div>
    </div>
  @else
    <div class="d-flex justify-content-center align-items-center flex-column text-danger" style="height: 500px; font-size:30px;">
      <span><i class="fas fa-exclamation-triangle"></i><span class=""> ATTENZIONE</span></span>
      <span class="text-info">L'utente non è più presente nel nostro database</span>
      <br>
        <form action="{{route('logout')}}" method="POST">
          @csrf
          <button type="submit" class="btn btn-outline-info btn-lg">torna alla login</button>
        </form>
  </div>
  @endif
@endsection
