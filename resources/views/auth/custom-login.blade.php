@extends('layouts.app')

@section('content')
  <div id="app" class="h-100">
      <main class="py-4 h-100">
        <form class="form-signin" action="{{route('login')}}" method="post">
          @csrf
          <div class="text-center mb-4">
            <img class="mb-4" src="{{asset('/imgs/logo.jpg')}}" alt="" width="130">
            <h1 class="h3 mb-3 font-weight-normal">NUMEROLOGIA</h1>
            <h6 class="nav-item d-md-none text-center">
              Accedi al tuo account per vedere la tua mappa numerologica<br>
                Non hai un'account?<a class="nav-link d-inline-block" href="{{ route('register') }}">{{ __('translate.register') }}</a>
            </h6>
          </div>
          <!-- email -->
          <div class="form-label-group">
            <input type="email" id="inputEmail" name="email" class="form-control @error('email') is-invalid @enderror @error('password') is-invalid @enderror" placeholder="Email address" required autofocus>
            <label for="inputEmail">Indirizzo email</label>
          </div>
          <!-- password -->
          <div class="form-label-group">
            <input type="password" id="inputPassword" name="password" class="form-control @error('password') is-invalid @enderror @error('email') is-invalid @enderror" placeholder="Password" required>
            <label for="inputPassword">Password</label>
          </div>
          @error('email')
              <span class="invalid-feedback d-block mb-4" role="alert" style="font-size:14px;">
                  <strong>Le credenziali inserite sono errate</strong>
              </span>
          @enderror
          <div class="checkbox mb-3">
            <label>
              <input type="checkbox" value="remember-me"> {{__('translate.remember')}}
            </label>
          </div>
          <button class="btn btn-lg btn-primary btn-block" type="submit">{{__('translate.login')}}</button>
          <p class="mt-5 mb-3 text-muted text-center">&copy; 2020</p>
        </form>
      </main>
  </div>
@stop
