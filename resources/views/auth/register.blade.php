@extends('layouts.app')

@section('content')
  <main class="py-4 h-100">
    <form method="POST" action="{{ route('register') }}" id="form-register">
      @csrf
      <div class="text-center mb-4">
        <img class="mb-4" src="{{asset('/imgs/logo.jpg')}}" alt="" width="130">
        <h1 class="h3 mb-3 font-weight-normal">NUMEROLOGIA</h1>
        <h6 class="nav-item d-md-none">Crea il tuo account personale per scoprire la tua mappa numerologica</br>
            Hai già un'account?<a class="nav-link d-inline-block" href="{{ route('login') }}">{{ __('translate.login') }}</a>
        </h6>
      </div>
      <input type="hidden" id="user_nom" name="user_nom" value="{{ optional(Auth::user())->id }}">
      <div class="form-group mx-0 row justify-content-center">
          {{-- NOME --}}
          <div class="col-md-3">
            <div class="form-label-group">
              <input id="name" type="text" name="name" class="form-control @error('name') is-invalid @enderror"  value="{{ old('name') }}" autocomplete="name" autofocus>
              <label for="name">Nome (*)</label>
            </div>
            @error('name')
                <span class="alert text-danger p-0" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
          {{-- COGNOME --}}
          <div class="col-md-3">
            <div class="form-label-group">
              <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" autocomplete="lastname">
              <label for="lastname">Cognome (*)</label>
            </div>
            @error('lastname')
                <span class="alert text-danger p-0" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
      </div>
      <div class="form-group mx-0 row justify-content-center">
      {{-- SECONDO NOME --}}
          <div class="col-md-3">
            <div class="form-label-group">
              <input id="second_name" type="text" name="second_name" class="form-control @error('second_name') is-invalid @enderror" value="{{ old('second_name') }}" autocomplete="second_name">
              <label for="second_name">Secondo Nome (se esistente)</label>
            </div>
            @error('second_name')
                <span class="alert text-danger p-0" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
          {{-- SECONDO COGNOME --}}
          <div class="col-md-3">
            <div class="form-label-group">
              <input id="second_lastname" type="text" class="form-control @error('second_lastname') is-invalid @enderror" name="second_lastname" value="{{ old('second_lastname') }}" autocomplete="second_lastname">
              <label for="second_lastname">Secondo cognome (se esistente)</label>
            </div>
            @error('second_lastname')
                <span class="alert text-danger p-0" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
      </div>
      {{-- DATA DI NASCITA + EMAIL --}}
      <div class="form-group mx-0 row justify-content-center">
        <div class="col-md-3 mb-3 mb-md-0">
          <div class="form-label-group">
            <input type="date" id="date_of_birth" name="date_of_birth" class="form-control @error('date_of_birth') is-invalid @enderror" value="{{ old('date_of_birth') }}" autocomplete="data di nascita">
            <label for="date_of_birth">Data di nascita (*)</label>
          </div>
          @error('date_of_birth')
            <span class="alert text-danger p-0" role="alert">
              <strong>{{ $message }}</strong>
            </span>
          @enderror
        </div>
        <div class="col-md-3">
          <div class="form-label-group">
            <input type="email" id="inputEmail" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" autocomplete="email">
            <label for="email">Indirizzo Email (*)</label>
          </div>
          @error('email')
              <span class="alert text-danger p-0" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
      </div>
      {{-- PASSWORD --}}
      <div class="form-group mx-0 row justify-content-center">
        <div class="col-md-6">
          <div class="form-label-group">
            <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror">
            <label for="password">Password (*)</label>
          </div>
          @error('password')
            <span class="alert text-danger p-0" role="alert">
              <strong>{{ $message }}</strong>
            </span>
          @enderror
        </div>
      </div>
      {{-- PASSWORD-CONFIRM --}}
      <div class="form-group mx-0 row justify-content-center">
        <div class="col-md-6">
          <div class="form-label-group">
            <input type="password" id="password-confirm" name="password-confirm" class="form-control @error('password-confirm') is-invalid @enderror">
            <label for="password-confirm">Conferma la password (*)</label>
          </div>
          @error('password-confirm')
            <span class="alert text-danger p-0" role="alert">
              <strong>{{ $message }}</strong>
            </span>
          @enderror
        </div>
      </div>
      {{-- BUTTON --}}
      <div class="form-group mx-0 row justify-content-center align-items-center">
        <div class="checkbox mb-3 col-md-6">
          <label>
            <input type="checkbox" value="remember-me" style="height:20px;vertical-align:-5px;"> <span class="text-muted">{{__('translate.remember')}}</span>
          </label>
        </div>
      </div>
      <div class="form-group mx-0 row justify-content-center">
        <div class="checkbox mb-3 col-md-6">
        <button class="btn btn-lg btn-primary btn-block" type="submit" id="save">{{__('translate.register')}}</button>
        <p class="mt-5 mb-3 text-muted text-center">&copy; 2020</p>
      </div>
    </div>
    </form>
  </main>

  @parent
  @section('js')
    <script type="text/javascript">
      $(document).ready(function() {
   
      });
    </script>
  @endsection

@endsection

  