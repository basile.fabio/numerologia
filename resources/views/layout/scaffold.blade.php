<!DOCTYPE html>
<html lang="it" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body>
    <div class="bg-secondary w-100">
      <div class="container">
        <a class="navbar-brand text-white" href="{{ url('/') }}">
          {{ config('app.name', 'Laravel') }}
        </a>

        @yield('content')

      </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
  </body>
</html>
