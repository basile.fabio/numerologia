@component('mail::message')
# Introduction

Ciao {{$user->name}}. Il tuo numero della vita è stato creato ed è:
<h2 style="text-align:center;"><span style="padding:5px 7px;color:green;border: 1px solid;border-radius:50%;">{{$number->numero_vita_free}}</span></h2>

@component('mail::button', ['url' => route('numboards')])
Vai alla dashboard
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
