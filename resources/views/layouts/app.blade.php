<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <script src="https://kit.fontawesome.com/3a8fd6600a.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link href="{{ asset('css/floating-labels.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('style')
</head>
<body>
    <div id="app" class="h-100">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="@guest {{ url('/') }} @endguest">
                    {{ config('app.name', 'Laravel') }}
                </a>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @auth
                          <li class="nav-item dropdown">
                              <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                  @php
                                     $number = App\Number::where('user_id', Auth::id())->first();
                                     //dd($number, empty($number)); 
                                  @endphp
                                  {{ !empty($number) ? "Ciao ".Auth::user()->name : "No user" }}
                              </a>
                              @if(!empty($number))
                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                  <i class="fas fa-user-circle fa-6x pb-1 text-center text-muted w-100"></i>
                                  <hr class="my-0 pt-1">
                                  <a class="dropdown-item p-0 text-right pr-3" href="{{ route('login') }}"
                                     onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                      {{ __('Logout') }}
                                  </a>

                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>
                              </div>
                              @endif
                          </li>
                          @elseif(Route::current()->getName() == 'login')
                              <li class="nav-item d-none d-md-block">
                                  Non hai un'account?<a class="nav-link d-inline-block" href="{{ route('register') }}">{{ __('translate.register') }}</a>
                              </li>
                          @elseif(Route::current()->getName() == 'register')
                            <li class="nav-item d-none d-md-block">
                                Hai già un'account?<a class="nav-link d-inline-block" href="{{ route('home') }}">{{ __('translate.login') }}</a>
                            </li>
                        @else
                          <li class="nav-item d-none d-md-block">
                              Non hai un'account?<a class="nav-link d-inline-block" href="{{ route('register') }}">{{ __('translate.register') }}</a>
                          </li>
                        @endauth
                    </ul>
                {{-- </div> --}}
            </div>
        </nav>



    <div class="container">

    @yield('content')

    </div>

    </div>
    @yield('js')
    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    
</body>
</html>
