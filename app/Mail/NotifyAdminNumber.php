<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Number;
use App\User;

class NotifyAdminNumber extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $number;
    public $numero_vita_free;
    /**
     * @var $number
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Number $number, User $user)
    {
        $this->user = $user;
        $this->number = $number;
        $this->numero_vita_free = $number->numero_vita_free;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.notifyadminnewnumber');
    }
}
