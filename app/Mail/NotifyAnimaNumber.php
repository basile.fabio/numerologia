<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Number;
use App\User;

class NotifyAnimaNumber extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $number;
    public $numero_anima;
    /**
     * @var $number
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Number $number, User $user)
    {
        $this->user = $user;
        $this->number = $number;
        $this->numero_anima = $number->numero_anima;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.notifyanimanumber');
    }
}
