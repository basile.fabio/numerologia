<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\Number;

class TestEmailMD extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $number;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Number $number)
    {
      $this->user = $user;
      $this->number = $number;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mails.testemailmd');
    }
}
