<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class number extends Model
{
    protected $fillable = [
      'number_name',
      'number_value',
      'user_id'
    ];


    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function getNumberAttribute() {
      $unsNumbs = unserialize($this->number_value);
      $created_at = Carbon::parse($this->created_at);
      $createdAt = Carbon::parse($this->created_at)->toDateTimeString();
      //$created_at_24 = strtotime($created_at->addHours('24')->toDateTimeString());
      //$created_at_48 = strtotime($created_at->addHours('48')->toDateTimeString());
      $created_at_24 = $created_at->addHours('24')->toDateTimeString();
      $created_at_48 = $created_at->addHours('24')->toDateTimeString();
      $created_at_72 = $created_at->addHours('24')->toDateTimeString();
      $created_at_96 = $created_at->addHours('24')->toDateTimeString();
      //$now = strtotime(Carbon::now("Europe/Rome"));
      $now = Carbon::now("Europe/Rome")->toDateTimeString();
      $numbs = [];
      if ($now >= $created_at_24) {
        if ( $this->payment == 1 && is_null($this->notified_amount)) {
          $numbs['numero_anima'] = $unsNumbs['anima'];
          // \Mail::to($event->user->email)->send(new NotifyAdminNumber($event->number, $event->user));
          $this->notified_amount ++;
          $this->save();
        }
      }

      if ($now >= $created_at_48) {
        if ( $this->payment == 1 && $this->notified_amount == 1) {
          $numbs['numero_anima'] = $unsNumbs['anima'];
          $numbs['numero_persona'] = $unsNumbs['persona'];
          // event:send mail to comunicate user that new number is unlocked
          $this->notified_amount ++;
          $this->save();
        }
      }

      if ($now >= $created_at_72) {
        if ( $this->payment == 1 && $this->notified_amount == 2) {
          $numbs['numero_anima'] = $unsNumbs['anima'];
          $numbs['numero_persona'] = $unsNumbs['persona'];
          $numbs['numero_destino'] = $unsNumbs['destino'];
          // event:send mail to comunicate user that new number is unlocked
          $this->notified_amount ++;
          $this->save();
        }
      }

      if ($now >= $created_at_96) {
        if ( $this->payment == 1 && $this->notified_amount == 3) {
          $numbs['numero_anima'] = $unsNumbs['anima'];
          $numbs['numero_persona'] = $unsNumbs['persona'];
          $numbs['numero_destino'] = $unsNumbs['destino'];
          $numbs['numero_espressione'] = $unsNumbs['espressione'];
          // event:send mail to comunicate user that new number is unlocked
          $this->save();
        }
      }
      
      return $numbs;
    }
}
