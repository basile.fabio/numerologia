<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckMails
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      $mails = [
        'yesmail@mail.it',
        'yesmail2@mail.it',
        'yesmail3@mail.it',
      ];
      // if (!in_array($request->email, $mails)) {
      //   dd("{$request->email} non può effettuare l'accesso");
      // }
      
      return $next($request);
    }
}
