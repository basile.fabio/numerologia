<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Number;
use Helper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Events\NewNumberCreated;
use App\Http\Middleware\CheckMails;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::NUMBOARDS;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('mails');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
      $messages = [
        'name.required' => 'Il nome è obbligatorio',
        'name.alpha' => 'Il nome deve contenere solo lettere',
        'lastname.required' => 'Il cognome è obbligatorio',
        'lastname.alpha' => 'Il nome deve contenere solo lettere',
        'second_name.alpha' => 'Il secondo nome deve contenere solo lettere',
        'second_lastname.alpha' => 'Il secondo cognome deve contenere solo lettere',
        'date_of_birth.required' => 'data di nascita obbligatoria',
        'email.required' => 'la Email è obbligatoria',
        'password.required' => 'fornire la password è obbligatorio',
        'password-confirm.required' => 'devi confermare la password',
      ];

        return Validator::make($data, [
            'name' => ['required', 'string', 'alpha', 'max:255'],
            'lastname' => ['required', 'string', 'alpha', 'max:255'],
            'second_name' => ['alpha', 'nullable'],
            'second_lastname' => ['alpha', 'nullable'],
            'date_of_birth' => ['required', 'date'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'password-confirm' => ['required','same:password'],
        ], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public static function create(array $data)
    {
        //CREA NUOVO USER
        $users = User::create([
            
            'name' => $data['name'],
            'second_name' => $data['second_name'],
            'lastname' => $data['lastname'],
            'second_lastname' => $data['second_lastname'],
            'date_of_birth' => $data['date_of_birth'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        // recupera array somma di vocali e consonanti per nome, cognome, secondo nome, secondo cognome e data di nascita.
        $inputs = Helper::calcInputSum($users);
        // recupera record Numeri user appena registrato
        $numbers = Number::where('number_name','numbers-'.$users->name)->first();
        $numbs = [];
        // calcolo totale dei vari genere numeri (es. anima, destino...)
        $free_vita = $inputs['sum_vowels_name'] + $inputs['sum_vowels_lastname'] + $inputs['sum_vowels_secondName'] + $inputs['sum_vowels_secondLastname'];//somma vocali nome+cognome
        $anima = $inputs['sum_consonants_name'] + $inputs['sum_consonants_lastname'] + $inputs['sum_consonants_secondName'] + $inputs['sum_consonants_secondLastname'];//somma consonanti nome+cognome
        $persona = $inputs['sum_vowels_name'] + $inputs['sum_vowels_lastname'] + $inputs['sum_vowels_secondName'] + $inputs['sum_vowels_secondLastname'] +
        $inputs['sum_consonants_name'] + $inputs['sum_consonants_lastname'] + $inputs['sum_consonants_secondName'] + $inputs['sum_consonants_secondLastname'];//somma vocali e consonanti nome+cognome
        $destino = $inputs['birth_sum'];//somma data di nascita
        $espressione = $anima + $persona;//somma $anima + $persona

        // array con numeri già ridotti ad una cifra
        $numbs['free_vita'] = Helper::roundToOneVal($free_vita);;
        $numbs['anima'] = Helper::roundToOneVal($anima);
        $numbs['persona'] = Helper::roundToOneVal($persona);
        $numbs['destino'] = Helper::roundToOneVal($destino);
        $numbs['espressione'] = Helper::roundToOneVal($espressione);
        // $numbs['free_vita'] = $free_vita;
        // $numbs['anima'] = $anima;
        // $numbs['persona'] = $persona;
        // $numbs['destino'] = $destino;
        // $numbs['espressione'] = $espressione;


        // creazione record sulla tabella number relativo all'utente appena registrato
        $name = 'numbers_'.$users->name . '_' . $users->id;
        
        $number = new Number;
        $number->create(
            [
                'number_name'   => $name,
                'number_value'  => serialize($numbs),
                'user_id'       => $users->id
            ]
        );

        return $users;
    }
}