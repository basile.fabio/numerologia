<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Number;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Carbon;
use App\Mail\NotifyAnimaNumber;
use App\Events\NewAnimaNumber;

class NumboardController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function numboard()
  {
    $user = User::where('id', Auth::id())->first();
    $numberName = 'numbers_'.$user->name . '_' . $user->id;
    $numbers = Number::where('number_name', $numberName)->first();
    
        // $noty = $numbers->notified_amount;
        // if ($noty == null) {
        //   $numbers->notified_amount ++;
        //   \Mail::to($user->email)->send(new NotifyAnimaNumber($numbers, $user));
        //   $numbers->save();
        // }
        
    return view('numboards.numboard', compact('numbers'));
  }
}
