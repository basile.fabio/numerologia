<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\NewAnimaNumber;

class NotifyAnimaNumber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewAnimaNumber $event
     * @return void
     */
    public function handle($event)
    {
        \Mail::to($event->user->email)->send(new NewAnimaNumber($event->number, $event->user));
    }
}
