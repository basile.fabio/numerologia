<?php

namespace App\Listeners;

use App\Events\NewNumberCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\User;
use App\Mail\NotifyAdminNumber;

class NotifyAdminNewNumber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewNumberCreated  $event
     * @return void
     */
    public function handle(NewNumberCreated $event)
    {
        \Mail::to($event->user->email)->send(new NotifyAdminNumber($event->number, $event->user));
        // Mail::send('Html.view', $data, function ($message) {
        //     $message->from('numerologia@info.com', 'APP Numerologia');
        //     $message->sender('numerologia@info.com', 'APP Numerologia');
        //     $message->to($event->user->email, $event->$user->username);
        //     $message->subject('Numero: ' . $numberName. ' sbloccato');
        // });

    }
}
