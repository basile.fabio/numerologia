<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\NumberCreatedNotified;
use App\User;
use App\Mail\NotifyAdminNumber;

class NotifyNumberCreatedNotified
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NumberCreatedNotified  $event
     * @return void
     */
    public function handle(NumberCreatedNotified $event)
    {
        \Mail::to($event->user->email)->send(new NotifyAdminNumber($event->number, $event->user));
    }
}
