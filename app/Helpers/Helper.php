<?php

namespace App\Helpers;
use App\number;
use App\User;

class Helper
{
  static protected $vowels = [
    'a' => 1,
    'e' => 5,
    'i' => 9,
    'o' => 6,
    'u' => 3,
  ];

  static protected $consonants = [
    'b' => 2,
    'c' => 3,
    'd' => 4,
    'f' => 6,
    'g' => 7,
    'h' => 8,
    'j' => 1,
    'k' => 2,
    'l' => 3,
    'm' => 4,
    'n' => 5,
    'p' => 7,
    'q' => 8,
    'r' => 9,
    's' => 1,
    't' => 2,
    'v' => 4,
    'w' => 5,
    'x' => 6,
    'y' => 7,
    'z' => 8,
  ];

  static public function calcInputSum(User $users)
  {
    //dd($users->second_lastname);
      // init variabili per calcolo somma finale
      $sum_vowels_name = 0;
      $sum_consonants_name = 0;

      $sum_vowels_lastname = 0;
      $sum_consonants_lastname = 0;

      $sum_vowels_secondName = 0;
      $sum_consonants_secondName = 0;


      $sum_vowels_secondLastname = 0;
      $sum_consonants_secondLastname = 0;

      $utenti = [
        'name' => $users->name,
        'lastname' => $users->lastname,
        'second_name' => $users->second_name,
        'second_lastname' => $users->second_lastname
      ];
      // foreach negli input inseriti dall'utente
      foreach ($utenti as $key => $utente) {
          // crea array della stringa del singolo input
          $arrInput = str_split($utente);
          // foreach nella stringa dell'input(su ogni singolo carattere)
          foreach ($arrInput as $input) {
              // se il singolo carattere si trova nell'array $vowels aggiungi il valore nella variabile in base a quale tipo di input si sta processando
              if (array_key_exists($input, self::$vowels)) {
                  switch ($key) {
                    case 'name':
                        $sum_vowels_name += self::$vowels[$input];
                        break;
                    case 'lastname':
                        $sum_vowels_lastname += self::$vowels[$input];
                        break;
                    case 'second_name':
                        $sum_vowels_secondName += self::$vowels[$input];
                        break;
                    case 'second_lastname':
                        $sum_vowels_secondLastname += self::$vowels[$input];
                        break;
                    default:
                        
                        break;
                  } 
              }
               // se il singolo carattere si trova nell'array $consonants aggiungi il valore nella variabile in base a quale tipo di input si sta processando
              if(array_key_exists($input, self::$consonants)) {
                
                  switch ($key) {
                    case 'name':
                        $sum_consonants_name += self::$consonants[$input];
                        break;
                    case 'lastname':
                        $sum_consonants_lastname += self::$consonants[$input];
                        break;
                    case 'second_name':
                        $sum_consonants_secondName += self::$consonants[$input];
                        break;
                    case 'second_lastname':
                        $sum_consonants_secondLastname += self::$consonants[$input];
                        break;
                    default:
                        
                        break;
                  }
                  
              }
          } 
      }
      // crea array del input: data di nascita
      $arrBirth = str_split($users->date_of_birth);
      // somma array $arrBirth
      $sum_birth = array_sum($arrBirth);
      // array da restituire
      $sums = [
         'sum_vowels_name' =>  self::roundToOneVal($sum_vowels_name) != 0 ? self::roundToOneVal($sum_vowels_name) : 0,
         'sum_consonants_name' =>  self::roundToOneVal($sum_consonants_name) != 0 ? self::roundToOneVal($sum_consonants_name) : 0,
         'sum_vowels_lastname' =>  self::roundToOneVal($sum_vowels_lastname) != 0 ? self::roundToOneVal($sum_vowels_lastname) : 0,
         'sum_consonants_lastname' =>  self::roundToOneVal($sum_consonants_lastname) != 0 ? self::roundToOneVal($sum_consonants_lastname) : 0,
         'sum_vowels_secondName' =>  self::roundToOneVal($sum_vowels_secondName) != 0 ? self::roundToOneVal($sum_vowels_secondName) : 0,
         'sum_consonants_secondName' =>  self::roundToOneVal($sum_consonants_secondName) != 0 ? self::roundToOneVal($sum_consonants_secondName) : 0,
         'sum_vowels_secondLastname' => self::roundToOneVal($sum_vowels_secondLastname) != 0 ? self::roundToOneVal($sum_vowels_secondLastname) : 0,
         'sum_consonants_secondLastname' => self::roundToOneVal($sum_consonants_secondLastname) != 0 ? self::roundToOneVal($sum_consonants_secondLastname) : 0,
         'birth_sum' =>  self::roundToOneVal($sum_birth) != 0 ? self::roundToOneVal($sum_birth) : 0,
      ];

      return $sums;
  }

  public static function roundToOneVal($result)
  {
    if ($result >=10) {
      $newVal = 0;//init
      $res = str_split($result);//output array
      $sum = array_sum($res);//somma valori array
      $newVal = $sum;//nuova somma ad una cifra
      if ($newVal >=10) {
        $newVal2 = 0;//init
        $res2 = str_split($newVal2);//output array
        $sum2 = array_sum($res2);//somma valori array
        $newVal2 = $sum2;//nuova somma ad una cifra
        
        return $newVal2;
      }
      return $newVal;
    }

    return $result;
  }
}
